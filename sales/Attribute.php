<?php 

// Avoid any time limit
set_time_limit(0);

// Avoid any memory limit
ini_set('memory_limit', -1);

// Include bootstrap code and Mage class
require_once 'app/Mage.php';

// Enable developer mode
Mage::setIsDeveloperMode(true);

// Set the default file creation mask
umask(0);

// Init application with default store
Mage::app();

$attributes = Mage::getSingleton('eav/config')
    ->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getAttributeCollection();

// Localize attribute label (if you need it)
$attributes->addStoreLabel(Mage::app()->getStore()->getId());

// Loop over all attributes
foreach ($attributes as $attr) {
    /* @var $attr Mage_Eav_Model_Entity_Attribute */

    $code = $attr->getAttributeCode();
    echo "<p class='{$code}'> <div> Code: {$code}\n";

    // If it is an attribute with predefined values
    if ($attr->usesSource()) {
        echo " source: {$attr->getSourceModel()}"; 
    }

    if ($attr->getisRequired()) {
      echo "  -required- </div> </p>"; 
    }
}