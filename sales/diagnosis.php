<?php 

// var_dump conf
ini_set('xdebug.var_display_max_depth', 5);
ini_set('xdebug.var_display_max_children', 256);
ini_set('xdebug.var_display_max_data', 1024);

// Avoid any time limit
set_time_limit(0);

// Avoid any memory limit
ini_set('memory_limit', -1);

// Include bootstrap code and Mage class
require_once 'app/Mage.php';

// Enable developer mode
Mage::setIsDeveloperMode(true);

// Set the default file creation mask
umask(0);

// Init application with default store
Mage::app();

  if (!$_GET["increment_id"]) {

    $_orders = Mage::getModel('sales/order')->getCollection();

    $order = $_orders->getLastItem();
  }else{
    $order =  Mage::getModel('sales/order')->loadByIncrementId($_GET["increment_id"]);
  }


$invoice = $order->getInvoiceCollection()->getLastItem();

$payment = $order->getPayment();

$getCustomerId = $order->getCustomerId();
$customer = Mage::getModel('customer/customer')->load($getCustomerId);

$billing = $order->getBillingAddress();
$shipping = $order->getShippingAddress();
#print_r(get_class_methods($order));


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Magento Searchs</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>


<div class="list-group col-sm-2">
<h3>Magento Diagnostic</h3>
  <a class="list-group-item" href="#" onclick="show('.order')">&nbsp; Order</a>
  <a class="list-group-item" href="#" onclick="show('.payment')"></i>&nbsp; Payment</a>
  <a class="list-group-item" href="#" onclick="show('.invoice')"></i>&nbsp; Invoice</a>
  <a class="list-group-item" href="#" onclick="show('.customer')"></i>&nbsp; Customer</a>
  <a class="list-group-item" href="#" onclick="show('.billing')"></i>&nbsp; Billing</a>
  <a class="list-group-item" href="#" onclick="show('.shipping')"></i>&nbsp; Shipping</a>

<br>
  <form class="form-inline" role="form">
    <div class="form-group">
      <label for="order">Order Increment Id:</label>
      <input type="text" class="form-control" name="increment_id" value="<?php echo $_GET["increment_id"]; ?>" placeholder="Increment Id">
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

<p>Reference <?php echo $order->getIncrementId(); ?></p>
<div class="container-fluid">
  <div class="row">
  	
    <div class="col-sm-4 order" style="background-color:lavender; display: none;"><label>Order</label><pre><?php var_dump($order->getData())?></pre></div>

    <div class="col-sm-4 invoice" style="background-color:lavender; display: none;"><label>Invoice</label><pre><?php var_dump($invoice->getData())?></pre></div>

    <div class="col-sm-4 payment" style="background-color:lavender; display: none;"> <label>Payment</label><pre><?php var_dump($payment->getData())?></pre></div>

    <div class="col-sm-4 customer" style="background-color:lavender; display: none;"><label>Customer</label><pre><?php var_dump($customer->getData())?></pre></div>

    <div class="col-sm-4 billing" style="background-color:lavender; display: none;"><label>Billing</label><pre><?php var_dump($billing->getData())?></pre></div>
    <div class="col-sm-4 shipping" style="background-color:lavender; display: none;"><label>Shipping</label><pre><?php var_dump($shipping->getData())?></pre></div>
  </div>
</div>

</body>
<script>
  function show(argument) {
    $('.col-sm-4').each(function(){
        $(this).hide()
    });
    $(argument).show();
  }

</script>
</html>