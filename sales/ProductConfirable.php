<?php 

// Avoid any time limit
set_time_limit(0);

// Avoid any memory limit
ini_set('memory_limit', -1);

// Include bootstrap code and Mage class
require_once 'app/Mage.php';

// Enable developer mode
Mage::setIsDeveloperMode(true);

// Set the default file creation mask
umask(0);

// Init application with default store
Mage::app();

$productId = 3947; //Its my product id
$product = Mage::getModel('catalog/product')->load($productId);


if($product->getTypeId() == "configurable"){

	$configurable= Mage::getModel('catalog/product_type_configurable')->setProduct($product);
	$simpleCollection = $configurable->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
	foreach($simpleCollection as $simpleProduct){
		echo "<hr>";
	    var_dump($simpleProduct->getData());
	}
}
