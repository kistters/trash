<?php 

// Avoid any time limit
set_time_limit(0);

// Avoid any memory limit
ini_set('memory_limit', -1);

// Include bootstrap code and Mage class
require_once 'app/Mage.php';

// Enable developer mode
Mage::setIsDeveloperMode(true);

// Set the default file creation mask
umask(0);

// Init application with default store
Mage::app();


$_orders = Mage::getModel('sales/order')->getCollection();

echo "<pre>";

echo "<span>methods Collection Orders</span> <br />";
print_r(get_class_methods($_orders));

echo "<span>methods Order</span> <br />";
print_r(get_class_methods($_orders->getLastItem()));

echo "<span>Orders Data</span> <br />";
while ($_order = $_orders->fetchItem()) {
	print_r($_order->getData());
}